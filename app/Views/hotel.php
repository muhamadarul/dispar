<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Udema a modern educational site template">
	<meta name="author" content="Ansonika">
	<title>Hotel</title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="https://dispar.bekasikab.go.id/assets/front/img/logo1.png" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-144x144-precomposed.png">

	<!-- GOOGLE WEB FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">

	<!-- BASE CSS -->
    <link rel="stylesheet" href="https://dispar.bekasikab.go.id/assets/plugins/fontawesome-free/css/all.min.css">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/style.css" rel="stylesheet">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/vendors.css" rel="stylesheet">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/icon_fonts/css/all_icons.min.css" rel="stylesheet">

	<!-- SPECIFIC CSS -->
	<link href="https://dispar.bekasikab.go.id/assets/front/layerslider/css/layerslider.css" rel="stylesheet">

	<!-- SPECIFIC CSS -->
	<link href="https://dispar.bekasikab.go.id/assets/front/css/blog.css" rel="stylesheet">

	<!-- YOUR CUSTOM CSS -->
	<link href="https://dispar.bekasikab.go.id/assets/front/css/custom.css" rel="stylesheet">
	<style>
		.vid-container {
			position: relative;
			padding-bottom: 2%;
			padding-top: 30px;
			height: 650px;
		}

	@media only screen and (max-width: 700px) {
    	.vid-container {
			height: 350px;
		}

    }
					/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a grey background color */
.prev:hover, .next:hover {
  background-color: #f1f1f1;
  color: black;
}
	</style>
	<style>
		/* Slider */
		.slick-slide {
			margin: 0px 20px;
		}

		.slick-slide img {
			width: 100%;
		}

		.slick-slider {
			position: relative;

			display: block;
			box-sizing: border-box;

			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;

			-webkit-touch-callout: none;
			-khtml-user-select: none;
			-ms-touch-action: pan-y;
			touch-action: pan-y;
			-webkit-tap-highlight-color: transparent;
		}

		.slick-list {
			position: relative;

			display: block;
			overflow: hidden;

			margin: 0;
			padding: 0;
		}

		.slick-list:focus {
			outline: none;
		}

		.slick-list.dragging {
			cursor: pointer;
			cursor: hand;
		}

		.slick-slider .slick-track,
		.slick-slider .slick-list {
			-webkit-transform: translate3d(0, 0, 0);
			-moz-transform: translate3d(0, 0, 0);
			-ms-transform: translate3d(0, 0, 0);
			-o-transform: translate3d(0, 0, 0);
			transform: translate3d(0, 0, 0);
		}

		.slick-track {
			position: relative;
			top: 0;
			left: 0;

			display: block;
		}

		.slick-track:before,
		.slick-track:after {
			display: table;

			content: '';
		}

		.slick-track:after {
			clear: both;
		}

		.slick-loading .slick-track {
			visibility: hidden;
		}

		.slick-slide {
			display: none;
			float: left;

			height: 100%;
			min-height: 1px;
		}

		[dir='rtl'] .slick-slide {
			float: right;
		}

		.slick-slide img {
			display: block;
		}

		.slick-slide.slick-loading img {
			display: none;
		}

		.slick-slide.dragging img {
			pointer-events: none;
		}

		.slick-initialized .slick-slide {
			display: block;
		}

		.slick-loading .slick-slide {
			visibility: hidden;
		}

		.slick-vertical .slick-slide {
			display: block;

			height: auto;

			border: 1px solid transparent;
		}

		.slick-arrow.slick-hidden {
			display: none;
		}
	</style>
    <style>
	.bs-vertical-slider .carousel-item-next.carousel-item-left,
        .bs-vertical-slider .carousel-item-prev.carousel-item-right {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

		.bs-vertical-slider .carousel-item-next,
		.bs-vertical-slider .active.carousel-item-right {
			-webkit-transform: translate3d(0, 100%, 0);
			transform: translate3d(0, 100% 0);
		}

		.bs-vertical-slider .carousel-item-prev,
		.bs-vertical-slider .active.carousel-item-left {
			-webkit-transform: translate3d(0, -100%, 0);
			transform: translate3d(0, -100%, 0);
		}

		.item img {
			width: 100%;
		}

		.col-sm-4 {
			background-color: #ccc;
		}

		.img-circle {
			height: 48px;
			width: 48px;
			border-radius: 50%;
			background-color: #0000ff;
			box-shadow: inset 1px 1px 1px 1px rgba(0, 0, 0, 0.5);
		}

		.img-circle.active {
			background-color: #ffff99;
		}
	</style>


</head>

<body>

	<div id="page">

		<header class="header menu_2">
			<div id="preloader">
				<div data-loader="circle-side"></div>
			</div><!-- /Preload -->
			<div id="logo">
				<a href="https://dispar.bekasikab.go.id/"><img src="https://dispar.bekasikab.go.id/assets/front/img/logo.png" width="250px" data-retina="true" alt=""></a>
			</div>
			<!-- /top_menu -->
			<a href="#menu" class="btn_mobile">
				<div class="hamburger hamburger--spin" id="hamburger">
					<div class="hamburger-box">
						<div class="hamburger-inner"></div>
					</div>
				</div>
			</a>
			<nav id="menu" class="main-menu">
				<ul>
					<li><span class="text2"><a href="https://dispar.bekasikab.go.id/">Beranda</a></span></li>
					<li><span><a href="">Profil</a></span>
						<ul>
							<li><a href="https://dispar.bekasikab.go.id/sejarah">Sejarah</a></li>
							<li><a href="https://dispar.bekasikab.go.id/visimisi">Visi & Misi</a></li>
    						<li><a href="https://dispar.bekasikab.go.id/struktur">Struktur Organisasi</a></li>
    						<li><a href="https://dispar.bekasikab.go.id/anggota">Keanggotaan</a></li>
						</ul>
					</li>
					<li><span><a href="">Media</a></span>
						<ul>
							<li><a href="https://dispar.bekasikab.go.id/galeri">Foto</a></li>
							<li><a href="https://dispar.bekasikab.go.id/video">Video</a></li>
						</ul>
					</li>
					<li><span><a href="">Informasi</a></span>
						<ul>
							<li><a href="https://dispar.bekasikab.go.id/berita">Berita</a></li>
							<li><a href="https://dispar.bekasikab.go.id/wisata">Wisata Unggulan</a></li>
                            <li><a href="https://dispar.bekasikab.go.id/wisata">Hotel</a></li>
                            <li><a href="https://dispar.bekasikab.go.id/wisata">Kuliner</a></li>
						</ul>
					</li>
					<li><span><a href="https://dispar.bekasikab.go.id/event">Event</a></span></li>
				</ul>
			</nav>
		</header>
		<!-- /header --><main>

	<div class="container margin_60_35">
		<div class="row">
			<div class="col-lg-9">
				<div class="bloglist singlepost">
					<div id="bs4-vertical-slide-carousel" class="carousel bs-vertical-slider slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#bs4-vertical-slide-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#bs4-vertical-slide-carousel" data-slide-to="1"></li>
                        <li data-target="#bs4-vertical-slide-carousel" data-slide-to="2"></li>
                        <li data-target="#bs4-vertical-slide-carousel" data-slide-to="3"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block mx-auto img-fluid" src="images/isoras.jpg" alt="Slide Number 1">
                            <!--Captions for the slides go here -->
                            <div class="carousel-caption text-danger d-none d-sm-block">
                                <h5>Vertical Carousel Demo</h5>
                                <p class="text-light">Slides moving vertically rather default horizontal
                                    <button class="btn btn-outline-info btn-lg">More info</button>
                                </p>
                            </div>
                            <!--Captions ending here for slide 3-->
                        </div>
                        <div class="carousel-item">
                            <img class="d-block mx-auto img-fluid" src="images/isora_01.jpg" alt="Slide Number 2">
                            <!--Captions for the slides go here -->
                            <div class="carousel-caption text-danger d-none d-sm-block">
                                <h5>Vertical Carousel Demo</h5>
                                <p class="text-light">Slides moving vertically rather default horizontal
                                    <button class="btn btn-outline-info btn-lg">More info</button>
                                </p>
                            </div>
                            <!--Captions ending here for slide 3-->
                        </div>
                        <div class="carousel-item">
                            <img class="d-block mx-auto img-fluid" src="images/isoras_02.jpg" alt="Slide Number 3">
                            <!--Captions for the slides go here -->
                            <div class="carousel-caption text-danger d-none d-sm-block">
                                <h5>Vertical Carousel Demo</h5>
                                <p class="text-light">Slides moving vertically rather default horizontal
                                    <button class="btn btn-outline-info btn-lg">More info</button>
                                </p>
                            </div>
                            <!--Captions ending here for slide 3-->
                        </div>
                        <div class="carousel-item">
                            <img class="d-block mx-auto img-fluid" src="images/isora_03.jpg" alt="Slide Number 4">
                            <!--Captions for the slides go here -->
                            <div class="carousel-caption text-danger d-none d-sm-block">
                                <h5>Vertical Carousel Demo</h5>
                                <p class="text-light">Slides moving vertically rather default horizontal
                                    <button class="btn btn-outline-info btn-lg">More info</button>
                                </p>
                            </div>
                            <!--Captions ending here for slide 3-->
                        </div>
                    </div>



                </div>
                <div class="text-center mt-4">
                    <!-- <button class="btn btn-success" id="btnPrev">Prev</button>
                    <button class="btn btn-danger" id="btnPause">Pause</button>
                    <button class="btn btn-success" id="btnNext">Next</button> -->

                    <button class="carousel-buttons">
                        <a data-target="#bs4-vertical-slide-carousel" data-slide-to="0" href="#">
                            <div class="img-circle active center-block">
                                <img class="carousel-icons-img" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-48.png" />
                            </div>
                        </a>
                    </button>

                    <button class="carousel-buttons">
                        <a data-target="#bs4-vertical-slide-carousel" data-slide-to="1" href="#">
                            <div class="img-circle center-block">
                                <img class="carousel-icons-img" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-48.png" />
                            </div>
                        </a>
                    </button>

                    <button class="carousel-buttons">
                        <a data-target="#bs4-vertical-slide-carousel" data-slide-to="2" href="#">
                            <div class="img-circle center-block">
                                <img class="carousel-icons-img" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-48.png" />
                            </div>
                        </a>
                    </button>
                    <button class="carousel-buttons">
                        <a data-target="#bs4-vertical-slide-carousel" data-slide-to="3" href="#">
                            <div class="img-circle center-block">
                                <img class="carousel-icons-img" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-48.png" />
                            </div>
                        </a>
                    </button>

                </div>

					<h1>ISORAS CIKARANG</h1>
					<div class="postmeta">
						<ul>
							<li><i class="icon_folder-alt"></i> Hotel</li>
							<li><i class="fas fa-map-marked"></i> Jl. Kemang Boulevard Kav.07 Lippo Cikarang, 17530 Cikarang, Indonesia </li>
						</ul>
					</div>
					<div class="a2a_kit a2a_kit_size_25 a2a_default_style">
						<a class="a2a_button_facebook"></a>
						<a class="a2a_button_twitter"></a>
						<a class="a2a_button_email"></a>
						<a class="a2a_button_whatsapp"></a>
						<a class="a2a_button_line"></a>
					</div>
					<script async src="https://static.addtoany.com/menu/page.js"></script>
					<br>
					<div class="post-content">
						<div class="dropcaps">
							<div>SISORAS CIKARANG terletak di Cikarang, 9 km dari Stadion Wibawa Mukti, dan menawarkan akomodasi dengan restoran, tempat parkir pribadi gratis, pusat kebugaran, serta lounge bersama. Hotel bintang 4 ini menyediakan layanan resepsionis 24 jam, layanan pramutamu, dan Wi-Fi gratis.</div>
                            <div><br></div>
                            <div>Semua kamarnya ber-AC serta dilengkapi dengan microwave, kulkas, teko, shower, pengering rambut, dan meja. Terdapat juga lemari pakaian, TV layar datar, dan kamar mandi bersama.<br>
Akomodasi ini menawarkan teras. <br>Bandara terdekat adalah Bandara Halim Perdanakusuma, 36 km dari ISORAS CIKARANG</div>
                       </div>
					</div>
					<!-- /post -->
				</div>
				<!-- /single-post -->


			</div>
			<!-- /col -->

			<aside class="col-lg-3">
				<!-- <div class="widget">
						<form>
							<div class="form-group">
								<input type="text" name="search" id="search" class="form-control" placeholder="Search...">
							</div>
							<button type="submit" id="submit" class="btn_1 rounded"> Search</button>
						</form>
					</div> -->
				<!-- /widget -->
				<div class="widget">
					<div class="widget-title">
						<h4>Hotel Terkait </h4>
					</div>
					<ul class="comments-list">
													<li>
								<div class="alignleft">
									<a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/8"><img src="https://dispar.bekasikab.go.id/assets/images/wisata_unggulan/taman-buaya-indonesia-jaya-e1480472452810-630x380.png" alt=""></a>
								</div>
								<small>Wisata Edukasi</small>
								<h3><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/8" title="">Taman Buaya</a></h3>
							</li>
													<li>
								<div class="alignleft">
									<a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/6"><img src="https://dispar.bekasikab.go.id/assets/images/wisata_unggulan/6.jpg" alt=""></a>
								</div>
								<small>Taman</small>
								<h3><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/6" title="">Taman Sehati</a></h3>
							</li>
													<li>
								<div class="alignleft">
									<a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/5"><img src="https://dispar.bekasikab.go.id/assets/images/wisata_unggulan/f1d3063d46e5f0da7cdccf18bd1d9b22.jpg" alt=""></a>
								</div>
								<small>Wisata Alam</small>
								<h3><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/5" title="">Pantai Muara Gembong</a></h3>
							</li>
											</ul>
				</div>
				<!-- /widget -->
			</aside>
			<!-- /aside -->

		</div>
		<!-- /row -->
		<div id="comments">
			<h5><b>Komentar</b></h5>
			<ul>
									<li>
						<div class="avatar">
							<img src="https://dispar.bekasikab.go.id/assets/front/img/user.png" alt="">
							</a>
						</div>
						<div class="comment_right clearfix">
							<div class="comment_info">
								By <a href="#">janBreatte@belan.website</a><span>|</span>17/04/2020							</div>
							<p>
								Viagra Brand At Low Price  <a href=https://viacialisns.com/#>Cialis</a> Mail Order Cialis  <a href=https://viacialisns.com/#>buy generic cialis</a> Side Affects Of Amoxicillin Clavulin  							</p>
						</div>
					</li>
									<li>
						<div class="avatar">
							<img src="https://dispar.bekasikab.go.id/assets/front/img/user.png" alt="">
							</a>
						</div>
						<div class="comment_right clearfix">
							<div class="comment_info">
								By <a href="#">janBreatte@belan.website</a><span>|</span>24/04/2020							</div>
							<p>
								Amoxicillin For Dental Implants  <a href=https://abcialisnews.com/#>Cialis</a> Kamagra Usage  <a href=https://abcialisnews.com/#>cheapest cialis 20mg</a> Where To Order Generic Stendra Worldwide  							</p>
						</div>
					</li>
							</ul>
		</div>

		<hr>


		<h5>Beri Komentar Disini</h5>
		<form action="https://dispar.bekasikab.go.id/komentar/tambah" method="post" accept-charset="utf-8">
		<input type="hidden" name="tabel" value="wisata">
		<input type="hidden" name="detail" value="detail_wisata">
		<input type="hidden" name="id_postingan" value="3">
		<div class="form-group">
			<input type="text" name="nama_lengkap" id="name2" class="form-control" placeholder="Masukan Nama" required>
		</div>
		<div class="form-group">
			<input type="email" name="email" id="email2" class="form-control" placeholder="Masukan Email" required>
		</div>
		<div class="form-group">
			<textarea class="form-control" name="isi" id="comments2" rows="6" placeholder="Isi komentar anda"></textarea>
		</div>
		<div class="form-group">
			<button type="submit" id="submit2" class="btn_1 rounded add_bottom_30"> Submit</button>
		</div>
		</form>
	</div>
	<!-- /container -->
</main>
<!--/main--><footer>
	<div class="container margin_120_90">
		<center>
			Dinas Pariwisata Bekasi Copyright AM &copy;<script>
				document.write(new Date().getFullYear());
			</script>
			<!-- <div id="copy">© 2019 Udema</div> -->
		</center>
	</div>
</footer>
<!--/footer-->
</div>
<!-- page -->

<!-- COMMON SCRIPTS -->
<script src="https://dispar.bekasikab.go.id/assets/front/js/jquery-2.2.4.min.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/js/common_scripts.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/js/main.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/assets/validate.js"></script>

<!-- SPECIFIC SCRIPTS -->
<script src="https://dispar.bekasikab.go.id/assets/front/layerslider/js/greensock.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/layerslider/js/layerslider.transitions.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script type="text/javascript">
	'use strict';
	$('#layerslider').layerSlider({
		autoStart: true,
		navButtons: false,
		navStartStop: false,
		showCircleTimer: false,
		responsive: true,
		responsiveUnder: 1280,
		layersContainer: 1200,
		skinsPath: 'layerslider/skins/'
		// Please make sure that you didn't forget to add a comma to the line endings
		// except the last line!
	});
</script>
<script>
 $('#bs4-vertical-slide-carousel').carousel({
    interval: 4000
})

$('#btnPrev').on('click', function () {
    $('#bs4-vertical-slide-carousel').carousel(0);
})

$('#btnPause').on('click', function () {
    $('#bs4-vertical-slide-carousel').carousel('pause');
})

$('#btnNext').on('click', function () {
    $('#bs4-vertical-slide-carousel').carousel(2);
})

$('#bs4-vertical-slide-carousel').on('slid.bs.carousel', function (event) {
    var nextactiveslide = $(event.relatedTarget).index();
    var $btns = $('.carousel-buttons');
    var $active = $btns.find("[data-slide-to='" + nextactiveslide + "']");
    $btns.find('.img-circle').removeClass('active');
    $active.find('.img-circle').addClass('active');
});
</script>

</body>

</html>
