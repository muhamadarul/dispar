<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Udema a modern educational site template">
	<meta name="author" content="Ansonika">
	<title>DINAS PARIWISATA KABUPATEN BEKASI</title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="https://dispar.bekasikab.go.id/assets/front/img/logo1.png" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="https://dispar.bekasikab.go.id/assets/front/img/apple-touch-icon-144x144-precomposed.png">

	<!-- GOOGLE WEB FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">

	<!-- BASE CSS -->
    <link rel="stylesheet" href="https://dispar.bekasikab.go.id/assets/plugins/fontawesome-free/css/all.min.css">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/style.css" rel="stylesheet">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/vendors.css" rel="stylesheet">
	<link href="https://dispar.bekasikab.go.id/assets/front/css/icon_fonts/css/all_icons.min.css" rel="stylesheet">

	<!-- SPECIFIC CSS -->
	<link href="https://dispar.bekasikab.go.id/assets/front/layerslider/css/layerslider.css" rel="stylesheet">

	<!-- SPECIFIC CSS -->
	<link href="https://dispar.bekasikab.go.id/assets/front/css/blog.css" rel="stylesheet">

	<!-- YOUR CUSTOM CSS -->
	<link href="https://dispar.bekasikab.go.id/assets/front/css/custom.css" rel="stylesheet">


<link rel="stylesheet" href="assets/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">

</head>

<body>

	<div id="page">

		<header class="header menu_2">
			<div id="preloader">
				<div data-loader="circle-side"></div>
			</div><!-- /Preload -->
			<div id="logo">
				<a href="https://dispar.bekasikab.go.id/"><img src="https://dispar.bekasikab.go.id/assets/front/img/logo.png" width="250px" data-retina="true" alt=""></a>
			</div>
			<!-- /top_menu -->
			<a href="#menu" class="btn_mobile">
				<div class="hamburger hamburger--spin" id="hamburger">
					<div class="hamburger-box">
						<div class="hamburger-inner"></div>
					</div>
				</div>
			</a>
			<nav id="menu" class="main-menu">
				<ul>
					<li><span class="text2"><a href="https://dispar.bekasikab.go.id/">Beranda</a></span></li>
					<li><span><a href="">Profil </a> </span>
						<ul>
							<li><a href="https://dispar.bekasikab.go.id/sejarah">Sejarah</a></li>
							<li><a href="https://dispar.bekasikab.go.id/visimisi">Visi & Misi</a></li>
    						<li><a href="https://dispar.bekasikab.go.id/struktur">Struktur Organisasi</a></li>
    						<li><a href="https://dispar.bekasikab.go.id/anggota">Keanggotaan</a></li>
						</ul>
					</li>
					<li><span><a href="">Media</a></span>
						<ul>
							<li><a href="https://dispar.bekasikab.go.id/galeri">Foto</a></li>
							<li><a href="https://dispar.bekasikab.go.id/video">Video</a></li>
						</ul>
					</li>
					<li><span><a href="">Informasi</a></span>
						<ul>
							<li><a href="https://dispar.bekasikab.go.id/berita">Berita</a></li>
							<li><a href="https://dispar.bekasikab.go.id/wisata">Wisata Unggulan</a></li>
                            <li><a href="https://dispar.bekasikab.go.id/wisata">Hotel</a></li>
                            <li><a href="https://dispar.bekasikab.go.id/wisata">Kuliner</a></li>
						</ul>
					</li>
					<li><span><a href="https://dispar.bekasikab.go.id/event">Event</a></span></li>
				</ul>
			</nav>
		</header>
		<!-- /header -->
<main>


	<!-- Slider -->
	<div id="full-slider-wrapper">
		<div id="layerslider" style="width:100%;height:750px;">
			<!-- first slide -->
							<div class="ls-slide" data-ls="slidedelay: 5000; transition2d:85;">
					<img src="https://dispar.bekasikab.go.id/assets/images/slide/6.jpg" class="ls-bg" alt="Slide background">
					<h3 class="ls-l slide_typo text1" style="top: 47%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Dinas Pariwisata Bekasi					<p class="ls-l slide_typo_2 text1" style="top:55%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">
						Bekasi Baru | Bekasi Bersih
					</p>
					<a class="ls-l btn_1 rounded" style="top:65%; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='https://dispar.bekasikab.go.id/berita'>Baca Berita Sekarang!</a>
				</div>
							<div class="ls-slide" data-ls="slidedelay: 5000; transition2d:85;">
					<img src="https://dispar.bekasikab.go.id/assets/images/slide/12.jpg" class="ls-bg" alt="Slide background">
					<h3 class="ls-l slide_typo text1" style="top: 47%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Dinas Pariwisata Bekasi					<p class="ls-l slide_typo_2 text1" style="top:55%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">
						Bekasi Baru | Bekasi Bersih
					</p>
					<a class="ls-l btn_1 rounded" style="top:65%; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='https://dispar.bekasikab.go.id/berita'>Baca Berita Sekarang!</a>
				</div>
							<div class="ls-slide" data-ls="slidedelay: 5000; transition2d:85;">
					<img src="https://dispar.bekasikab.go.id/assets/images/slide/13.jpg" class="ls-bg" alt="Slide background">
					<h3 class="ls-l slide_typo text1" style="top: 47%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Dinas Pariwisata Bekasi					<p class="ls-l slide_typo_2 text1" style="top:55%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">
						Bekasi Baru | Bekasi Bersih
					</p>
					<a class="ls-l btn_1 rounded" style="top:65%; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='https://dispar.bekasikab.go.id/berita'>Baca Berita Sekarang!</a>
				</div>

		</div>
	</div>
	<!-- End layerslider -->

    <div class="onoffswitch3">
    <input type="checkbox" name="onoffswitch3" class="onoffswitch3-checkbox" id="myonoffswitch3" checked>
    <label class="onoffswitch3-label" for="myonoffswitch3">
        <span class="onoffswitch3-inner">
            <span class="onoffswitch3-active">
                <marquee class="scroll-text">Avengers: Infinity War's Iron Spider Suit May Use Bleeding Edge Tech  <span class="glyphicon glyphicon-forward"></span> Russo brothers ask for fans not to spoil Avengers: Infinity War <span class="glyphicon glyphicon-forward"></span>  Bucky's Arm Miraculously Regenerates On Avengers: Infinity War Poster</marquee>
                <span class="onoffswitch3-switch">BREAKING NEWS <span class="glyphicon glyphicon-remove"></span></span>
            </span>
        </span>
    </label>
</div>

<div class="container"><br>
    <div class="main_title_2">
			<span><em></em></span>
			<h2>Hotel</h2>
			<p>Hotel & Penginapan di Kabupaten Bekasi.</p>
            <p>
            	<a href="">All</a> |
             	<a href=""> Cikarang Pusat</a> |
                <a href=""> Cikarang Timur</a> |
                <a href=""> Cikarang Barat</a> |
                <a href=""> Cikarang Utara</a> |
                <a href=""> Tambun </a> |
                <a href=""> Cibitung </a>
            </p>
		</div>

    <div class="row">
        <div class="col-md-12">
            <div id="news-slider12" class="owl-carousel">
                <div class="post-slide12">
                    <div class="post-img">
                        <span class="over-layer"></span>
                        <img src="images/isoras.jpg" alt="">
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="<?php echo base_url('hotel');?>">ISORAS CIKARANG <span><img src="images/bintang4.png" style="width:20%"></span></a></h3>
                        <span class="post-date" style="font-size:10px">Jl. Kemang Boulevard Kav.07 Lippo Cikarang, 17530 Cikarang, Indonesia <img src="images/antarjemput.png" style="width:50%"></span>
                        <p class="post-description">
                            ISORAS CIKARANG terletak di Cikarang, 9 km dari Stadion Wibawa Mukti, ..... <a href="">Lihat Detail</a>
                        </p>
                        <p></p>
                    </div>
                </div>

                <div class="post-slide12">
                    <div class="post-img">
                        <span class="over-layer"></span>
                        <img src="images/herper.jpg" alt="">
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="#">Harper Lippo Cikarang by ASTON <span><img src="images/bintang4.png" style="width:20%"></span></a> </h3>
                        <span class="post-date" style="font-size:10px">Jl. Mataram Kav. 37-39 Cibatu Cikarang - West Java, 17530 Cikarang, Indonesia <img src="images/antarjemput.png" style="width:50%"></span>
                        <p class="post-description">
                            Terletak di Cikarang, Harper Cikarang - West Java menawarkan sebuah kolam renang ..... <a href="">Lihat Detail</a>
                        </p>
                    </div>
                </div>

                <div class="post-slide12">
                    <div class="post-img">
                        <span class="over-layer"></span>
                        <img src="images/ayola.jpg" alt="">
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="#">Hotel AYOLA Lippo Cikarang <span><img src="images/bintang4.png" style="width:20%"></span></a></h3>
                        <span class="post-date" style="font-size:10px">Jn Sriwijaya Kav 19 Lippo Cikarang, Cikarang Selatan, 17550, Indonesia <img src="images/antarjemput.png" style="width:50%"></span>
                        <p class="post-description">
                            Hotel AYOLA Lippo Cikarang menyediakan akomodasi yang berjarak 22 km dari Grand Galaxy Park dan 19 km dari Blu Plaza ..... <a href="">Lihat Detail</a>
                        </p>
                    </div>
                </div>

                <div class="post-slide12">
                    <div class="post-img">
                        <span class="over-layer"></span>
                        <img src="images/batiqa.jpg" alt="">
                    </div>
                    <div class="post-review">
                        <h3 class="post-title"><a href="#">Batiqa Hotel Jababeka <span><img src="images/bintan3.png" style="width:15%"></span></a></h3>
                        <span class="post-date" style="font-size:10px">Jl. Niaga Raya Kawasan Industri Jababeka II, Blok CC3A, 17530 Cikarang, Indonesia <img src="images/antarjemput.png" style="width:50%"></span>
                        <p class="post-description">
                            Terletak di Kawasan Industri Jababeka dan berjarak 5 menit berkendara dari pusat kota, Batiqa Hotel Jababeka ..... <a href="">Lihat Detail</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="bg_color_1">
 <div class="container margin_30_95">
    <div class="main_title_2">
			<span><em></em></span>
			<h2>Kuliner</h2>
			<p>Kuliner & Jajanan di Kabupaten Bekasi.</p>
            <p>
            	<a href="">All</a> |
             	<a href=""> Sayur</a> |
                <a href=""> Cemilan</a> |
                <a href=""> Minuman</a>
            </p>
		</div>
    <div class="row">
        <div class="col-md-12">
            <div id="news-slider7" class="owl-carousel">
                <div class="post-slide7">
                    <div class="post-img">
                        <img src="images/gabuspucung.jpg" alt="">
                        <span class="icons">
                            <img src="images/gabuspucung.jpg" alt="">
                        </span>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title">Sayur Gabus Pucung</h3>
                        <p class="post-description">Namanya sih sayur gabus pucung, tapi termasuk lauk pauk, bukan sayuran. Kuahnya pekat, mirip rawon tapi menggunakan ikan gabus goreng </p>
                        <a href="#" class="read">read more<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                <div class="post-slide7">
                    <div class="post-img">
                        <img src="images/birpletok.jpg" alt="">
                        <span class="icons">
                            <img src="images/birpletok.jpg" alt="">
                        </span>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title">Bir Pletok</h3>
                        <p class="post-description">Gak bikin mabuk, yang ini namanya bir pletok. Terbuat dari rempah-rempah, bir ini bisa menghangatkan badan. </p>
                        <a href="#" class="read">read more<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                <div class="post-slide7">
                    <div class="post-img">
                        <img src="images/bandengrorod.jpg" alt="">
                        <span class="icons">
                            <img src="images/bandengrorod.jpg" alt="">
                        </span>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title">Bandeng Rorod</h3>
                        <p class="post-description">Ekstra lembut, Bandeng Rorod ini gak punya duri lho. Racikan bumbu di dalamnya bikin nagih. </p>
                        <a href="#" class="read">read more<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                <div class="post-slide7">
                    <div class="post-img">
                        <img src="images/dodol.jpg" alt="">
                        <span class="icons">
                            <img src="images/dodol.jpg" alt="">
                        </span>
                    </div>
                    <div class="post-review">
                        <h3 class="post-title">Dodol Betawi</h3>
                        <p class="post-description">Jadi sajian wajib saat Idul Fitri, dodol Betawi ini gak boleh kamu lewatkan. Rasanya legit, cocok banget jadi teman minum teh. </p>
                        <a href="#" class="read">read more<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </div>
</div>

	<div class="container-fluid margin_120_0">
		<div class="main_title_2">
			<span><em></em></span>
			<h2>Wisata Favorit</h2>
			<p>Kenali Wisata Favorit di Kabupaten Bekasi.</p>
		</div>
		<div id="reccomended" class="owl-carousel owl-theme">
			<!-- /item -->
							<div class="item">
					<div class="box_grid">
						<figure>
							<!-- <a href="#0" class="wish_bt"></a> -->
							<a href="#"><img src="https://dispar.bekasikab.go.id/assets/images/wisata_unggulan/3.jpg" class="img-fluid" alt=""></a>
							<div class="preview"><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/3"><span>Lihat Lokasi</span></a></div>
						</figure>
						<div class="wrapper">
							<small>Wisata Alam</small>
							<h3>Kawung Tilu</h3>
						</div>
						<ul>
							<li></li>
							<li><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/3">Detail Lokasi</a></li>
						</ul>
					</div>
				</div>
							<div class="item">
					<div class="box_grid">
						<figure>
							<!-- <a href="#0" class="wish_bt"></a> -->
							<a href="#"><img src="https://dispar.bekasikab.go.id/assets/images/wisata_unggulan/f1d3063d46e5f0da7cdccf18bd1d9b22.jpg" class="img-fluid" alt=""></a>
							<div class="preview"><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/5"><span>Lihat Lokasi</span></a></div>
						</figure>
						<div class="wrapper">
							<small>Wisata Alam</small>
							<h3>Pantai Muara Gembong</h3>
						</div>
						<ul>
							<li></li>
							<li><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/5">Detail Lokasi</a></li>
						</ul>
					</div>
				</div>
							<div class="item">
					<div class="box_grid">
						<figure>
							<!-- <a href="#0" class="wish_bt"></a> -->
							<a href="#"><img src="https://dispar.bekasikab.go.id/assets/images/wisata_unggulan/6.jpg" class="img-fluid" alt=""></a>
							<div class="preview"><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/6"><span>Lihat Lokasi</span></a></div>
						</figure>
						<div class="wrapper">
							<small>Taman</small>
							<h3>Taman Sehati</h3>
						</div>
						<ul>
							<li></li>
							<li><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/6">Detail Lokasi</a></li>
						</ul>
					</div>
				</div>
							<div class="item">
					<div class="box_grid">
						<figure>
							<!-- <a href="#0" class="wish_bt"></a> -->
							<a href="#"><img src="https://dispar.bekasikab.go.id/assets/images/wisata_unggulan/taman-buaya-indonesia-jaya-e1480472452810-630x380.png" class="img-fluid" alt=""></a>
							<div class="preview"><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/8"><span>Lihat Lokasi</span></a></div>
						</figure>
						<div class="wrapper">
							<small>Wisata Edukasi</small>
							<h3>Taman Buaya</h3>
						</div>
						<ul>
							<li></li>
							<li><a href="https://dispar.bekasikab.go.id/wisata/detail_wisata/8">Detail Lokasi</a></li>
						</ul>
					</div>
				</div>

			<!-- /item -->
		</div>
		<!-- /carousel -->
		<div class="container">
			<p class="btn_home_align"><a href="https://dispar.bekasikab.go.id/wisata" class="btn_1 rounded">Lihat Semua Wisata</a></p>
		</div>
		<!-- /container -->

	</div>
	<!-- /container -->
<div class="bg_color_1">
	<div class="container margin_30_95">
		<div class="main_title_2">
			<span><em></em></span>
			<h2>Galeri Dinas Pariwisata</h2>
		</div>
		<div class="row">
							<div class="col-lg-4 col-md-6 wow" data-wow-offset="150">
					<a href="#0" class="grid_item">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
							<img src="https://dispar.bekasikab.go.id/assets/images/galeri/30.jpg" class="img-fluid" alt="">
							<div class="info">
								<small><i class="ti-layers"></i>
									Taman Sehati Wibawa Mukti								</small>
							</div>
						</figure>
						<!--  -->
					</a>
				</div>
							<div class="col-lg-4 col-md-6 wow" data-wow-offset="150">
					<a href="#0" class="grid_item">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
							<img src="https://dispar.bekasikab.go.id/assets/images/galeri/12.jpg" class="img-fluid" alt="">
							<div class="info">
								<small><i class="ti-layers"></i>
									Kawung Tilu								</small>
							</div>
						</figure>
						<!--  -->
					</a>
				</div>
							<div class="col-lg-4 col-md-6 wow" data-wow-offset="150">
					<a href="#0" class="grid_item">
						<figure class="block-reveal">
							<div class="block-horizzontal"></div>
							<img src="https://dispar.bekasikab.go.id/assets/images/galeri/lippo.jpg" class="img-fluid" alt="">
							<div class="info">
								<small><i class="ti-layers"></i>
									Wisata Air								</small>
							</div>
						</figure>
						<!--  -->
					</a>
				</div>
						<div class="container">
				<p class="btn_home_align"><a href="https://dispar.bekasikab.go.id/galeri" class="btn_1 rounded">Lihat Semua Galeri</a></p>
			</div>
			<!-- /grid_item -->
		</div>
		<!-- /row -->
         </div>
		<!-- /container -->
            </div>

         <div class="bg_color_2">
		<div class="container margin_120_95">
			<div class="main_title_2">
				<span><em></em></span>
				<h2>Video</h2>
				<p>Kanal Youtube Video</p>
			</div>

            <div class="slideshow-container">

              <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/snTo536a9E8?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/HpgB0SYx9Ak?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/Ivx-RXgIYq0?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/aFytfcgDe0g?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/A8RhBcM0j24?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/N_KuTHYJb5g?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/P2QHMbfaAjQ?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/yO4W8yKQfLg?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/V8IODnSUsks?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/1mkiLLTzIqk?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/0dfN2iYVXi0?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/Nhvt7c0-zJA?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/4AhIKi8_XQI?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/ET44MLTzVpQ?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/uvQTjl-35gs?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/CkXfmk6r1NM?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/TweAKpJ-ynI?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/mcd8W6zP5l8?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>
  <div class="mySlides1">
   <div class="vid-container">
				<iframe width="560" id="vid-frame" height="315" src="https://youtube.com/embed/mdtvMygvTps?autoplay=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
  </div>

  <a class="prev" onClick="plusSlides(-1, 0)">&#10094;</a>
  <a class="next" onClick="plusSlides(1, 0)">&#10095;</a>
</div>
			<p class="btn_home_align"><a href="https://dispar.bekasikab.go.id/video" class="btn_1 rounded">Lihat Semua Video</a></p>
		</div>
		<!-- /container -->
	</div>
	<!-- /bg_color_1 -->

		<div class="bg_color_1">
			<div class="container margin_120_95">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>Event</h2>
					<p>Kumpulan Event Terkini</p>
				</div>
				<div class="row">
											<div class="col-lg-6">
							<a class="box_news" href="https://dispar.bekasikab.go.id/event/detail_event/23">
								<figure><img src="https://dispar.bekasikab.go.id/assets/images/event/281e79f9-060f-4d35-b4e4-316edd07c71b.jpg" alt="">
									<figcaption><strong>22</strong>Nov</figcaption>
								</figure>
								<ul>
									<li>Bekasi</li>
									<li>Friday, 22 Nov 2019</li>
								</ul>
								<h4>Bekasi Tourism Exhibition</h4>
								<p>Bang mpok, Dinas Pariwisata Kabupaten Bekasi ke......[Selengkapnya]</p>
							</a>
						</div>
											<div class="col-lg-6">
							<a class="box_news" href="https://dispar.bekasikab.go.id/event/detail_event/0">
								<figure><img src="https://dispar.bekasikab.go.id/assets/images/event/66a36806-7277-477c-9460-edaf51b39566.jpg" alt="">
									<figcaption><strong>26</strong>Oct</figcaption>
								</figure>
								<ul>
									<li>Bekasi</li>
									<li>Saturday, 26 Oct 2019</li>
								</ul>
								<h4>GOWES WISATA</h4>
								<p>Abang mpok inilah rute untuk acara Gowes besok ......[Selengkapnya]</p>
							</a>
						</div>
											<div class="col-lg-6">
							<a class="box_news" href="https://dispar.bekasikab.go.id/event/detail_event/22">
								<figure><img src="https://dispar.bekasikab.go.id/assets/images/event/e1805d2a-47d5-4f02-888b-555739f8f174.jpg" alt="">
									<figcaption><strong>26</strong>Oct</figcaption>
								</figure>
								<ul>
									<li>Bekasi</li>
									<li>Saturday, 26 Oct 2019</li>
								</ul>
								<h4>PAGELARAN WAYANG GOLEK</h4>
								<p>Bang mpok, dalam rangka mempromosikan obyek wis......[Selengkapnya]</p>
							</a>
						</div>
											<div class="col-lg-6">
							<a class="box_news" href="https://dispar.bekasikab.go.id/event/detail_event/21">
								<figure><img src="https://dispar.bekasikab.go.id/assets/images/event/d1f7425e-7b29-4c09-baf6-82ea457b5efd.jpg" alt="">
									<figcaption><strong>20</strong>Sep</figcaption>
								</figure>
								<ul>
									<li>Bekasi</li>
									<li>Friday, 20 Sep 2019</li>
								</ul>
								<h4>PEKAN RAYA BEKASI</h4>
								<p>Abang mpok, nyok kite ramein Pekan Raya Bekasi ......[Selengkapnya]</p>
							</a>
						</div>
									</div>
				<!-- /row -->
				<p class="btn_home_align"><a href="https://dispar.bekasikab.go.id/event" class="btn_1 rounded">Lihat Semua Event</a></p>
			</div>

			<!-- /container -->
		</div>
		<!-- /bg_color_1 -->

		<div class="bg_color_2">
			<div class="container margin_120_95">
				<div class="main_title_2">
					<span><em></em></span>
					<h2>Berita</h2>
					<p>Seputar Berita Terkini di Kabupaten Bekasi</p>
				</div>
				<div class="row">
											<div class="col-xl-4 col-lg-6 col-md-6">
							<div class="box_grid wow">
								<figure class="block-reveal">
									<div class="block-horizzontal"></div>
									<a href="https://dispar.bekasikab.go.id/berita/detail_berita/170"><img src="https://dispar.bekasikab.go.id/assets/images/berita/170.png" class="img-fluid" alt=""></a>
								</figure>
								<div class="wrapper">
									<small>Pengumuman</small>
									<h3>Grafik Penyebaran kasus Covid 19 di daerah Kabupaten Bekasi pekan terakhir masih meningkat.</h3>
									<p>Grafik Penyebaran kasus Covid 19 di daerah Kabu......[Selengkapnya]</p>
								</div>
								<ul>
									<li><i class="icon_calendar"></i>&nbsp;Wednesday, 08 Apr 2020</li>
									<li><a href="https://dispar.bekasikab.go.id/berita/detail_berita/170">Selengkapnya</a></li>
								</ul>
							</div>
						</div>
											<div class="col-xl-4 col-lg-6 col-md-6">
							<div class="box_grid wow">
								<figure class="block-reveal">
									<div class="block-horizzontal"></div>
									<a href="https://dispar.bekasikab.go.id/berita/detail_berita/169"><img src="https://dispar.bekasikab.go.id/assets/images/berita/169.png" class="img-fluid" alt=""></a>
								</figure>
								<div class="wrapper">
									<small>Pengumuman</small>
									<h3>Karyawan Dinas Pariwisata Kabupaten Bekasi menerapkan Social Distancing dan berjemur</h3>
									<p>Upaya berbudaya jaga jarak dan berjemur khusus ......[Selengkapnya]</p>
								</div>
								<ul>
									<li><i class="icon_calendar"></i>&nbsp;Wednesday, 08 Apr 2020</li>
									<li><a href="https://dispar.bekasikab.go.id/berita/detail_berita/169">Selengkapnya</a></li>
								</ul>
							</div>
						</div>
											<div class="col-xl-4 col-lg-6 col-md-6">
							<div class="box_grid wow">
								<figure class="block-reveal">
									<div class="block-horizzontal"></div>
									<a href="https://dispar.bekasikab.go.id/berita/detail_berita/168"><img src="https://dispar.bekasikab.go.id/assets/images/berita/168.png" class="img-fluid" alt=""></a>
								</figure>
								<div class="wrapper">
									<small>Pengumuman</small>
									<h3>Dinas Periwisata ikut berkonsentrasi Upaya pencegahan penyebaran viris covid 19 baik di lingkungan kantor</h3>
									<p>Dinas Periwisata ikut berkonsentrasi Upaya penc......[Selengkapnya]</p>
								</div>
								<ul>
									<li><i class="icon_calendar"></i>&nbsp;Wednesday, 08 Apr 2020</li>
									<li><a href="https://dispar.bekasikab.go.id/berita/detail_berita/168">Selengkapnya</a></li>
								</ul>
							</div>
						</div>

				</div>
				<!-- /row -->
				<p class="btn_home_align"><a href="https://dispar.bekasikab.go.id/berita" class="btn_1 rounded">Lihat Semua Berita</a></p>
			</div>
			<!-- /container -->
		</div>
		<!-- /bg_color_2 -->

		<div class="container">
  <h3>Support Our  Partners/ Our Clients</h3>
   <section class="customer-logos slider">
      <div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"></div>
      <div class="slide"><img src="http://www.webcoderskull.com/img/logo.png"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
      <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>
   </section><br><br>

</div>

</main>
<!-- /main -->


 <script>
var slideIndex = [1,1];
var slideId = ["mySlides1"]
showSlides(1, 0);
showSlides(1, 1);

function plusSlides(n, no) {
  showSlides(slideIndex[no] += n, no);
}

function showSlides(n, no) {
  var i;
  var x = document.getElementsByClassName(slideId[no]);
  if (n > x.length) {slideIndex[no] = 1}
  if (n < 1) {slideIndex[no] = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  x[slideIndex[no]-1].style.display = "block";
}
</script><footer>
	<div class="container margin_120_90">
		<center>
			Dinas Pariwisata Bekasi Copyright AM &copy;<script>
				document.write(new Date().getFullYear());
			</script>
			<!-- <div id="copy">© 2019 Udema</div> -->
		</center>
	</div>
</footer>
<!--/footer-->
</div>
<!-- page -->

<!-- COMMON SCRIPTS -->
<script src="https://dispar.bekasikab.go.id/assets/front/js/jquery-2.2.4.min.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/js/common_scripts.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/js/main.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/assets/validate.js"></script>

<!-- SPECIFIC SCRIPTS -->
<script src="https://dispar.bekasikab.go.id/assets/front/layerslider/js/greensock.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/layerslider/js/layerslider.transitions.js"></script>
<script src="https://dispar.bekasikab.go.id/assets/front/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script type="text/javascript">
	'use strict';
	$('#layerslider').layerSlider({
		autoStart: true,
		navButtons: false,
		navStartStop: false,
		showCircleTimer: false,
		responsive: true,
		responsiveUnder: 1280,
		layersContainer: 1200,
		skinsPath: 'layerslider/skins/'
		// Please make sure that you didn't forget to add a comma to the line endings
		// except the last line!
	});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
    $('.customer-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<script>
$(document).ready(function() {
    $("#news-slider").owlCarousel({
        items : 2,
        itemsDesktop : [1199,2],
        itemsMobile : [600,1],
        pagination :true,
        autoPlay : true
    });

    $("#news-slider2").owlCarousel({
        items:3,
        itemsDesktop:[1199,2],
        itemsDesktopSmall:[980,2],
        itemsMobile:[600,1],
        pagination:false,
        navigationText:false,
        autoPlay:true
    });

    $("#news-slider3").owlCarousel({
        items:3,
        itemsDesktop:[1199,2],
        itemsDesktopSmall:[1000,2],
        itemsMobile:[700,1],
        pagination:false,
        navigationText:false,
        autoPlay:true
    });

    $("#news-slider4").owlCarousel({
        items:3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[1000,2],
        itemsMobile:[600,1],
        pagination:false,
        navigationText:false,
        autoPlay:true
    });

    $("#news-slider5").owlCarousel({
        items:3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[1000,2],
        itemsMobile:[650,1],
        pagination:false,
        navigationText:false,
        autoPlay:true
    });

    $("#news-slider6").owlCarousel({
        items : 3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        pagination:false,
        navigationText:false
    });

    $("#news-slider7").owlCarousel({
        items : 3,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [1000,2],
        itemsMobile : [650,1],
        pagination :false,
        autoPlay : true
    });

    $("#news-slider8").owlCarousel({
        items : 3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        autoPlay:true
    });

    $("#news-slider9").owlCarousel({
        items : 3,
        itemsDesktop:[1199,2],
        itemsDesktopSmall:[980,2],
        itemsTablet:[650,1],
        pagination:false,
        navigation:true,
        navigationText:["",""]
    });

    $("#news-slider10").owlCarousel({
        items : 4,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        navigation:true,
        navigationText:["",""],
        pagination:true,
        autoPlay:true
    });

    $("#news-slider11").owlCarousel({
        items : 4,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        pagination:true,
        autoPlay:true
    });

    $("#news-slider12").owlCarousel({
        items : 2,
        itemsDesktop:[1199,2],
        itemsDesktopSmall:[980,1],
        itemsTablet: [600,1],
        itemsMobile : [550,1],
        pagination:true,
        autoPlay:true
    });

    $("#news-slider13").owlCarousel({
        navigation : false,
        pagination : true,
        items : 3,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [600,1],
        navigationText : ["",""]
    });

    $("#news-slider14").owlCarousel({
        items : 4,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[980,2],
        itemsMobile : [550,1],
        pagination:false,
        autoPlay:true
    });
});
</script>

</body>

</html>
